
import matplotlib.pyplot as plt
import math 
from numpy import exp,arange,random,zeros,linspace,array

def F(x1,x2):
    return x2**2 - math.cos(x1 - x2**2 - x2)

Nx1, Nx2 = 100, 50

idx, idy = 0,0

xx1, xx2 = linspace(-5,5,Nx1), linspace(-2,2,Nx2)
ZList = []
for x1 in xx2:
    ZList.append( [F(x2,x1) for x2 in xx1] )
    
Z = array([z for z in ZList])    
    
plt.contour(xx1,xx2, Z, 20)
plt.axis('equal')
plt.show()

#def F( x ): # x est un array
 #   x1 , x2 = x [0], x [1] # Les indices de Python commencent à 0.
  #  return x2 **2 - math.cos ( x1 - x2 **2 - x2 )


