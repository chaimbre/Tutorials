# importing the required module
import matplotlib.pyplot as plt
import math 
from numpy import exp,arange,random,zeros
from numpy.core.defchararray import array
from numpy.core.function_base import linspace
from numpy.linalg import norm

M = 100 # nombre de données
alpha0, alpha1 = 3,2

Xi = random.rand(M)*2-1 # M nombres aléatoires entre -1 et 1
Wi = random.rand(M)*2-1 # Le bruit
Yi = alpha0 + alpha1*Xi + Wi

# On affiche les valeurs
#plt.plot(Xi,Yi,'o')
#plt.show()  

def E(A):
    a0, a1 = A[0], A[1]
    Y = a0 + a1*Xi
    return norm(Y - Yi)**2/M

xxi = linspace(0,5,100)
yyi = linspace(0,5,100)

Mx = array([array([1,x]) for x in Xi])
P = 5
Pi = range(0,P)
Mxp = array( [[array([x**p]) for p in Pi] for x in Xi ]   )
print (Mxp)



def getPoly(A):
    return [a*x**p for a,p in enumerate(A) for x in Xi]