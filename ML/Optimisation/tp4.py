# importing the required module
import matplotlib.pyplot as plt
import math 
from numpy import exp,arange,random,zeros
from numpy.core.defchararray import array
from numpy.core.function_base import linspace
from numpy.linalg import norm

def F(x):
    x1 = x[0]
    x2 = x[1] 
    return x1**4-2*x1**2+2*x2**2+x1+2*x1**2*x2

def dF(X):
    x1 = X[0]
    x2 = X[1]
    print(x1,x2)
    return array( [4*x1**3-4*x1**2 + 1 + 4*x1*x2, 4*x2+2*x1**2] )

print(dF([-1,0]))

def descenteGradient(dF, x0, tau=0.05, tol=1e-6, Niter=1000):
    xn,L = x0,[]
    for n in range(Niter):
        print("xn", xn)
        dFxn = dF(xn)
        if norm(dFxn) < tol:
            return xn, L
        L.append(xn)
        xn = xn - tau*dF(xn)
    print("Probleme, la descente de gradient n'a pas convergée après", Niter,'itérations')
    return xn, L

xA,LA = descenteGradient(dF, array([-1, 0]))
xB,LB = descenteGradient(dF, array([1,-1]))

print('Point xA found in %d iterations'%len(LA))
print('Point xB found in %d iterations'%len(LB))

xx1 = linspace(-2,2,100)
xx2 = linspace(-2,1,100)
Z = array([[F(array([x1,x2])) for x1 in xx1 ] for x2 in xx2])

plt.contour(xx1,xx2,Z,100)
plt.plot([x[0] for x in LA], [x[1] for x in LA], '.-r')
plt.plot([x[0] for x in LB], [x[1] for x in LB], '.-b')