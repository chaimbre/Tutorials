# importing the required module
import matplotlib.pyplot as plt
import math 
from numpy import exp,arange,random,zeros
from numpy.core.defchararray import array
from numpy.core.function_base import linspace
from numpy.linalg import norm


def gradientDFC(F, num, eps=1e-5):
    def dE(x):
        v = zeros(num)
        for i in range(num):
            ei = zeros(num)
            ei[i] = 1
            v[i] = (F(x+eps*ei)-F(x-eps*ei))/(2*eps)
        return v
    return dE

def F(x): 
    return x[0]**2 + x[1]**2

#x = random.rand(2)
#nablaFx = gradientDFC(F, 2, 1e-5)(x)
#print(nablaFx)
#assert(max(abs(nablaFx - 2*x))) < 1e-8, "Erreur dans la fonction gradientDFC"

def gradConstStep(gradF, x0, alpha, niter=1000, tol=1e-6 ):
    xn, L = x0, []
    for n in range(niter):
        L.append(xn)
        if norm(gradF(xn)) < tol:
            return xn, L
        xn = xn - alpha*gradF(xn)

def F2(x): 
    return x[0]**2 - math.cos(x[1])

xx1 = linspace(-3,3,100)
xx2 = linspace(-3,3,100)

gradF = lambda x : gradientDFC(F2, 2, 1e-5)(x)
arr = zeros(2) 
arr[0] = 2
arr[1] = 1.5

M = 100 # nombre de données
alpha0, alpha1 = 3,2 # les nombres à trouver

Xi = random.rand(M)*2-1 # M nombres aléatoires entre -1 et 1
Wi = random.rand(M)*2-1 # Le bruit
Yi = alpha0 + alpha1*Xi + Wi

# On affiche les valeurs
#plt.plot(Xi,Yi,'o')
#plt.show()  

def E(A):
    a0, a1 = A[0], A[1]
    Y = a0 + a1*Xi
    return norm(Y - Yi)**2/M

xxi = linspace(0,5,100)
yyi = linspace(0,5,100)

Z = [[E(array([a0,a1])) for a0 in xxi] for a1 in yyi]

plt.contour(xxi,yyi,Z,30)
plt.show()


def RegLin_GPC(tau, A0=array([0,0])):
    gradE = gradientDFC(E,2)
    return gradConstStep(gradE,A0,tau)

res = RegLin_GPC(0.5,array([5,5]))  



def plot_CV_GPC(tau):
    An, L = RegLin_GPC(tau)
    contour(aa0,aa1,Z,30) # Afficher 30 lignes de niveau
    plot([A[0] for A in L], [A[1] for A in L],'o-')
    title("Convergence en %d itérations"%len(L))

tau = 0.3
plot_CV_GPC(tau)