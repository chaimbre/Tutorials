# importing the required module
import matplotlib.pyplot as plt
import math
from numpy import exp,arange


def f(x):
 return (x-2)**2+3

def df(x):
    return 2*x-4

def tan(x0):
    return lambda x : f(x0) + df(x0)*(x-x0)
# x axis value)
xvals = arange(-2,7,0.1)

dy = [df(x) for x in xvals]

#plt.plot(xvals, f(xvals))
#plt.plot(xvals, df(xvals))
#plt.plot(xvals, tan(4)(xvals))
#plt.plot(xvals,f(xvals)/df(xvals))


  
# naming the x axis
plt.xlabel('x - axis')
# naming the y axis
plt.ylabel('y - axis')
  
# giving a title to my graph
plt.title('My first graph!')

# function to show the plot
plt.show()



def newton (f , df , x0 , tol =1e-6 , Niter =1000):
    xn , L = x0 , [] # Initialisation
    for n in range ( Niter ) :
        fxn = f( xn )
        if abs ( fxn ) < tol :
            return xn, L
        L.append( xn )
        xn = xn - fxn/df(xn)
    print ( "Erreur , l ’ algorithme n ’a pas convergé après " , Niter , " itérations " )

fs = math.sin
fc = math.cos
#xn, L = newton(fs, fc, 6)
#print("value found ", xn, "value ", "serie ", L)


#b/ Ecrire une fonction puissanceInverse(X,n) qui renvoie X^1/n en résolvant l’équation x^n = X.
def ppow(n,X):
    return lambda x : x**n-X

def dppow(n):
    return lambda x : n*x**(n-1)

def puissanceInverse( X, n, x0 ):
    return newton( ppow(n,X), dppow(n), x0 )

#pn, S = puissanceInverse( 10000, 4, 11 )
#print("value found ", pn, "value ", "serie ", S)

def secante( f, x0, x1, tol = 1e-6, niter=1000 ):
    xn = x1 
    xn_1 = x0
    L = []
    for n in range(niter):
        print( f(xn) )
        if abs(f(xn)) < tol:
            return xn, L
        xn1 = xn - f(xn)*( (xn-xn_1) / (f(xn)-f(xn_1)) )
        L.append(xn)
        xn_1 = xn
        xn = xn1
        
xn, L = secante(fs, 3, 4 )
print("value found ", xn, "value ", "serie ", L)


