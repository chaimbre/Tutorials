import './App.css';
import { useEffect, useState } from 'react';


const react = [
  {
    title: "React",
    url: "http://react.js.org",
    author: "toto",
    points: 5,
    objectID: 0,
  },
  {
    title: "Redux",
    url: "http://redux.js.org",
    author: "totux",
    points: 3,
    objectID: 1,
  }
]

function Button(props) {
  return (
    <button
      className={props.className || ''}
      onClick={props.onClick}
      type="button"
    >
      {props.children}
    </button>
  )
}

export function ItemComponent(props) {
  const { title, url, author, points, objectID } = props.item; 
  return (
    <div key={objectID}>
      <span>
        <a href={url}>{title}</a>
      </span>
      <span>{author}</span>
      <span>{points}</span>
      <span>
        <Button onClick={()=> props.onRemove(objectID) }>
          Remove
        </Button>
      </span>
    </div>
  )
}

export function SearchComponent(props) {
  const { searchTerm, onChange, onSubmit, children} = props;
  return (
    <form> 
      {children}
      <input 
        type="text"
        value={searchTerm}
        onChange={(ev)=>onChange(ev)}
      />
    <input 
        type="button"
        onClick={onSubmit}
      />
    </form>
  )
}

export function TableComponent(props) {
  return ( 
    props.list.map( item =>
      <ItemComponent onRemove={props.onRemove} item={item}/> ) 
  );
}

const DEFAULT_QUERY = 'redux';
const PATH_BASE = 'https://hn.algolia.com/api/v1';
const PATH_SEARCH = '/search';
const PARAM_SEARCH = 'query=';
const PARAM_BASE = 'page='

function App() {
  const [results,setResults] = useState(null);
  const [searchTerm,setSearchTerm] = useState(DEFAULT_QUERY);
  const [page,setPage] = useState(0);
  const [error,setError] = useState(null);
  const [searchKey,setSearchKey] = useState(DEFAULT_QUERY);
 
  function setNewResult(res) {
    const prevHits = results && results[searchKey] ? results[searchKey].hits : [];
    const newHits = [...res.hits, ...prevHits];

    const newRes =  {...results, [searchKey]: { hits: newHits , page } };
    console.log( newRes );
    setResults( newRes);
  }

  function fetchSearchTopStories(searchTerm) {
    const url = `${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${searchTerm}&${PARAM_BASE}${page}`;
    console.log( "url", url)
    fetch( url )
      .then(r => r.json())
      .then(res => { setNewResult(res); } )
      .catch( error => setError(error) ) 
  }

  useEffect( () => {
    fetchSearchTopStories(searchKey);
  },[page])

  function onRemove(id) {
    if (results && results[searchKey]) {
      const hits = results[searchKey].hits.filter( elt => elt.objectID != id );
      setResults( {...results, [searchKey]: { ...results[searchKey], hits : hits } } )
    }    
  }

  function onSearchChange(event) {
    setSearchKey(searchTerm);
    setSearchTerm( event.target.value );
  }

  function onSearchSubmit(event) {
    fetchSearchTopStories( searchTerm );
    event.preventDefault();
  }

  function isSearchedTerm(st) { 
    return item => item.title.toLowerCase().includes(st.toLowerCase());
  }
  
  return (
    <div className="App">
      <SearchComponent 
        searchTerm={searchTerm} 
        onChange={onSearchChange}
        onSubmit={onSearchSubmit}
      >
      Search
      </SearchComponent>
      {    
        error ?  <p>Something went wrong </p> 
              : ( results && results[searchKey] && results[searchKey].hits ?
                <TableComponent list={results[searchKey].hits} onRemove={onRemove} />
                : null )
      }
      <div className="interactions">
        <Button onClick={() => {setPage(page+1)} }>
          More
        </Button>
      </div>
    </div>
  );  
}
//filter(isSearchedTerm(searchTerm))
export default App;
