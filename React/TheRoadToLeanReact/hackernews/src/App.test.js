import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import App, {SearchComponent, Button, Table} from './App';

describe( 'App', () => {
  it('renders without crashing', () => {
    const div = document.createElement( 'div' );
    ReactDOM.render( <App />,div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot', () => {
    const comp = renderer.create( <App />);
    const tree = comp.toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe('Search',() => {
  it('renders without crashing', () => {
    const div = document.createElement( 'div' );
    ReactDOM.render( <SearchComponent>Search</SearchComponent>,div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('has a valid snapshot', () => {
    const comp = renderer.create( 
      <SearchComponent>Search</SearchComponent>);
    const tree = comp.toJSON();
    expect(tree).toMatchSnapshot();
  });
})

