import { useState } from "react"
import {InputField,InputCheckField} from "./BaseComponents"

const items = [ 
    {category: "Sporting Goods", price: "$49.99", stocked: true, name: "Football"},
    {category: "Sporting Goods", price: "$9.99", stocked: true, name: "Baseball"},
    {category: "Sporting Goods", price: "$29.99", stocked: false, name: "Basketball"},
    {category: "Electronics", price: "$99.99", stocked: true, name: "iPod Touch"},
    {category: "Electronics", price: "$399.99", stocked: false, name: "iPhone 5"},
    {category: "Electronics", price: "$199.99", stocked: true, name: "Nexus 7"}
]

const ProductRow = ({name,value}) => {
    return (
        <tr>
            <td>{name}</td>
            <td>{value}</td>
        </tr>
    )
}

const ProductCategoryRow = ({title}) => {
    return (
        <tr>
            <th colSpan="2">
                {title}
            </th>
        </tr>
    )
}

const SearchField = ({searchTerm,isShowStockOnly,handleSearchTermChange,handleShowStockOnlyChecked}) => {
    
    function onSearchChange(e) {
        handleSearchTermChange(e.target.value);
    }

    function onShowStockOnlyChecked(e) {
        handleShowStockOnlyChecked(e.target.checked);
    }

    return (
        <>
        <div> <InputField 
                name={"Search"}
                value={searchTerm}
                onChange={onSearchChange}/> 
        </div>
        <div> <InputCheckField 
                name={"Only show product in stock"} 
                checked={isShowStockOnly}
                onChange={onShowStockOnlyChecked}/> 
        </div>
        </>
    );
}

const hasItemPattern = (pattern) => {
    return (item) => item.name.toLowerCase().includes(pattern.toLowerCase());
}

const isInStock = (item) =>  item.stocked

export const TableContent = ({list,pattern="",isShowStockOnly}) => {
    let prevcategory = null;
    let row = []
    list.filter( it => 
            { return hasItemPattern(pattern)(it) && (isShowStockOnly ? isInStock(it) : true) } )
        .forEach( it => {
        if ( it.category !== prevcategory )
            row.push( <ProductCategoryRow title={it.category}/> );
        row.push( <ProductRow name={it.name} value={it.price}/> );
        prevcategory = it.category;
    });

    return (
        <table>
            <thead>
                <tr>
                    <th> name </th>
                    <th> price </th>
                </tr>
            </thead>    
            <tbody>{ row }</tbody>
        </table>
    )
}

export const Table = () => {
    const [pattern,setPattern] = useState("");
    const [isShowStockOnly,setShowStockOnly] = useState(false);

    function handleSearchChange(term) {
        setPattern(term);
    } 

    function handleShowStockOnlyChecked( yn ) {
        setShowStockOnly(yn);
    }

    return (
        <>
            <SearchField 
                searchTerm={pattern}
                isShowStockOnly={isShowStockOnly}
                handleSearchTermChange={handleSearchChange} 
                handleShowStockOnlyChecked={handleShowStockOnlyChecked}
            />
            <TableContent 
                list={items} 
                pattern={pattern} 
                isShowStockOnly={isShowStockOnly}
            />
        </>
    )
}