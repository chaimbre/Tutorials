import React, {useState} from 'react';
import {InputField} from './BaseComponents'

const FahrenheitToDegree = (v) => (v -32)*5/9;
const DegreeToFahrenheit = (v) => 9/5*v +32;


export function BoilingVerdict({unit1,unit2,value1=0,value2=0}) {

    const [unit1Val,setUnit1Val] = useState(value1)
    const [unit2Val,setUnit2Val] = useState(value2)

    const isBoiling = () => { 
        return unit1 === "degree" ? unit1Val >= 100 : unit2Val >= 100; 
    };

    const unit1ToUnit2 = (v) => FahrenheitToDegree(v);
    const unit2ToUnit1 = (v) => DegreeToFahrenheit(v);

    const handleChange = (e) => { 
        let val = parseFloat(e.target.value);
        if ( isNaN(val) ) val = 0;
        if (e.target.name === unit1) {
            setUnit1Val(val);
            setUnit2Val(unit1ToUnit2(val));
        }
        else {
            setUnit2Val(val);
            setUnit1Val(unit2ToUnit1(val));
        }
    }

    return (
        <div className="">
            <InputField name={unit1} value={unit1Val} onChange={handleChange}/>
            <InputField name={unit2} value={unit2Val} onChange={handleChange}/>
            <>
                { isBoiling() ? "Water boils" : "too cold" }
            </>
        </div> 
    )
}