export function ComponentWithLabel(Component) {
    
    return (props) => (
        <div>   
            <label>{props.name}</label>
            <Component {...props}/>
        </div>    
    )
}

export function InputBaseField({name,onChange,value,children}) {
    return <input type="text" name={name} value={value} onChange={onChange}/>
}

export function InputBaseCheckField({name,onChange,value,children}) {
    return <input type="checkbox" name={name} checked={value} onChange={onChange}/>
}

export const InputField = ComponentWithLabel(InputBaseField);
export const InputCheckField = ComponentWithLabel(InputBaseCheckField);


